/*
1) Напишіть, як ви розумієте рекурсію. Навіщо вона використовується на практиці?

Рекурсія - це коли функція викликає саму себе як підзадачу для вирішення більшої задачі.
Цей процес може продовжуватися доти, доки не буде досягнуте базовий випадок,
коли виклик функції припиняється і починається розгортання стека викликів.
*/

let numb = ""
let exponent = ""


do {
    numb = +prompt("Enter a number");
    exponent = +prompt("Enter the exponent");
} while (isNaN(numb) && isNaN(exponent));

function pow(numb, exp) {
    if (exp === 1) {
        return numb;
    } else {
        return numb * pow(numb, exp - 1);
    }
}
let result = pow(numb, exponent);
alert(result);

